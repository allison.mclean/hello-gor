#!/usr/bin/env nextflow
nextflow.preview.dsl = 2

// include './modules/util' params(project_path: params.project_path, containers: params.containers)

process call_variant_qc {
    echo true
    container params.containers.gorpcontainer    

        input:
            path(vqsr)

        // output:
        //     path 'variant_qc_metrics.txt'
        //     path 'vqslod_pass_zero_snps.txt'

        script:
        """
        echo "Hello World!"
        # python3 '$workflow.projectDir/variant_qc.py' --input '${vqsr}' --output ${'variant_qc_metrics.txt'} --passsnps ${'vqslod_pass_zero_snps.txt'}
        """

        }


workflow {
    log.info("Starting Hello World")


    vqsr_pass = call_variant_qc(params.vqsr)

    // publish:
    // vqsr_pass to: "$dev_cache_prefix/joint-genotyping-qc/$params.runid/", mode: 'copy'

}
