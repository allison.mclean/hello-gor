
# Hello World

## 1) How to run it using the workflow service

```
nextcode workflow run --local . custom abbvie_analysis 
```

# Takes only 19 seconds... Go celebrate!

```
nextcode workflow run --local . custom abbvie_analysis 
Job 1404 has been submitted
Executing nextcode workflow job 1404 watch...

Job status: PENDING...
Job status: STARTED.
Job has completed in 0:00:13 with status: COMPLETED
Executing nextcode workflow job 1404 view...
job_id              1404
pipeline_name       (custom)
project             abbvie_analysis
submitted           2021-03-16 12:31:06
completed           2021-03-16 12:31:20
duration            0:00:13
context             None
nextflow ID         0a53cbfa-f97a-43a3-9316-585cdbf93286
nextflow Pod        nextflowexecutor-26a4226e (gorkube)
work dir            /mnt/csa/workflow/0001404
script_name         None
revision            (latest)
user                raony.guimaraes@genuitysci.com
status              COMPLETED
processes           Completed: 1
```