def create_gorpipe_command(command) {
    def escaped_command = command.replaceAll("\"","\\\\\"")
    """
        export GOR_GORPIPE_OPTS="-Xmx65536m"
        gor_cache_dir=\$(pwd)/gor_cache_\$(basename \$(mktemp -u))
        mkdir \$gor_cache_dir
        export TASK_WORKDIR=\$(pwd)
        gorpipe \"$escaped_command\" -stacktrace -workers 26 -cachedir \$gor_cache_dir
    """
}

def check_required_inputs(required_list, input) {
    def missing_params = required_list - required_list.intersect(input.keySet())
    if (missing_params) {
        error "The following input parameters are missing: $missing_params"
    }
}

def projectizeFile(filename) {
    if (filename?.startsWith("/")) {
        file("${filename}")
    } else if (filename?.startsWith("s3://")) {
        filename
    } else {
        file("${params.project_path}/${filename}")
    }
}

//native process that write the supplied parameters to a file
process writeParamsToFile {
    input:
    val parameters

    output:
    path p_file, emit: paramFile

    exec:
    p_file = file("${task.workDir}/params.txt")

    for (p in parameters) {
        if (p.key != "csa_api_password" && p.key != "csa_api_user" && p.key != "user_id") {
            p_file << "${p.key}:${p.value}\n"
        }
    }
}

process writeWorkflowInfoFile {
    output:
    path wf_file, emit: workflowInfoFile

    exec:
    wf_file = file("${task.workDir}/workflow.txt")
    wf_file << "repo: ${workflow.repository}\n"
    wf_file << "name: ${workflow.runName}\n"
    wf_file << "script: ${workflow.scriptName}\n"
    wf_file << "sha: ${workflow.commitId}\n"
    wf_file << "version: ${workflow.revision}\n"
}

process create_dictionary {
    input:
    val dict_name
    val id_file_pairs
    val file_prefix

    output:
    path dictionary_file

    exec:
    dictionary_file = file("${task.workDir}/$dict_name")

    id_file_pairs.each() {
        dictionary_file << ("${file_prefix}${it}\n")
    }
}